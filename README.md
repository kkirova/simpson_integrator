This is a project on creating a numerical integrator using the Simpson rule. 

In order to create the executable, one have to create an empty folder build, `mkdir build`,  cd in it, `cd build` and run `cmake ..` in it. 

This command generates the make file in the folder according to the two CMakeLists.txt files provided in the lib and in the main directories. 

Then, one need to run `cmake build --` or equivalently `make` to create the executable, simpsonIntegrator. 

Then, one can run the program with `./simpsonIntegrator` at which they will be prompted to enter an integer number 
corresponding to the number of bins. 

The programme prints the analytical and numerical results for 2 functions.  
