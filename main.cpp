/* Programming Techniques for Scientific Simulations, HS 2020
 * Caller for simpson library
 */

#include "lib/simpson.hpp"

#include <cmath>
#include <iostream>
#include <fstream>

double evaluate_sin(double x) {
    return std::sin(x);
}

double evaluate_polynomial(double x) {
    return x * (1.0 - x);
}

int main() {

  std::cout << "Integrator maximum resolution?" << std::endl;
  size_t N;
  std::cin >> N;

    std::cout << "The numerical value of the integral of x(1-x) is: "
         << integrate(0.0, 1.0, N, evaluate_polynomial) << std::endl;
    std::cout << "The analytical value of the integral of x(1-x) is: " << 1.0 / 6.0 << std::endl;

    std::cout << "The numerical value of the integral of sin(x) is: " << integrate(0.0, 3.1415, N, evaluate_sin)
         << std::endl;
    std::cout << "The analytical value of the integral of sin(x) is: " << 2.0 << std::endl;


  return 0;
}



